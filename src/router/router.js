import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from '../pages/home'

import LiveIndex from '../pages/live/index'
import TravelIndex from '../pages/live/travel/index'
import TravelTime from '../pages/live/travel/time'
import TravelCity from '../pages/live/travel/city'
import LoseWeight from '../pages/live/loseWeight/index'

import WorkIndex from '../pages/work/index'
import TechnologyIndex from '../pages/work/Technology/index'
import RecordIndex from '../pages/work/record/index'
import BlogIndex from '../pages/work/blog/index'

import ReduxIndex from '../pages/reduxDemo/index'

const BasicRoute = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={ Home }/>

            <Route exact path="/work" component={ WorkIndex }/>
            <Route exact path="/work/technology" component={ TechnologyIndex }/>
            <Route exact path="/work/record" component={ RecordIndex }/>
            <Route exact path="/work/blog" component={ BlogIndex }/>

            <Route exact path="/live" component={ LiveIndex }/>
            <Route exact path="/live/travel" component={ TravelIndex }/>
            <Route exact path="/live/travel/time" component={ TravelTime }/>
            <Route exact path="/live/travel/city" component={ TravelCity }/>
            <Route exact path="/live/loseWeight" component={ LoseWeight }/>

            <Route exact path="/redux" component={ ReduxIndex }/>
        </Switch>
    </BrowserRouter>
)


export default BasicRoute