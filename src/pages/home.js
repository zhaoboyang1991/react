import React from 'react'
import axios from 'axios'
import '../style/home.css'
import { Carousel } from 'antd'

export default class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      moduleArr: [
        {
          name: '工作',
          key: 'work'
        },
        {
          name: '生活',
          key: 'live'
        }
      ],
      weather: {
        city: '',
        detail: {}
      }
    }

    this.toModule = this.toModule.bind(this)
  }

  componentDidMount () {
    axios.get('https://www.tianqiapi.com/api/?version=v1').then((data) => {
      let weather = {
        city: data.data.city,
        detail: data.data.data[0]
      }
      this.setState({
        weather: weather
      })
    })
  }
  toModule(key) {
    this.props.history.push(`/${key}`)
  }

  render() {
    // 天气
    const weather = <div className="weather-wrap">
      <div className="paddingB10  font-size20 textAlignR city">{ this.state.weather.city }</div>
      <div className="flex-space date">
        <span>{ this.state.weather.detail.date }</span>
        <span>{ this.state.weather.detail.week }</span>
      </div>
      <span className="font-size76 margin0 currentTem">{ this.state.weather.detail.tem }</span>
      <div className="flex-space importantContent">
        <span className="">{ this.state.weather.detail.wea }</span>
        <div className="">
          <span>{ this.state.weather.detail.tem2 }</span>~~
          <span>{ this.state.weather.detail.tem1 }</span>  
        </div>
      </div>
      <span>空气污染指数：{ this.state.weather.detail.air }({ this.state.weather.detail.air_level })</span>
    </div>
    // 个人信息
    const personalInformation = <div className="personalInfor">
      <div className="marginAuto">
        <p className="textAlignC">赵泊洋</p>
        <p className="textAlignC">男</p>
        <p className="textAlignC">1991-03-01</p>
        <p className="textAlignC">江苏海洋大学（电子信息工程2011-2015）</p>
        <p className="textAlignC">web前端工程师</p>
      </div>
    </div>
    // 菜单
    const module = this.state.moduleArr.map((item) => {
      return <div className={ `cursor item item_${item.key}` } onClick={ this.toModule.bind(this, item.key) } key={ item.key }>{ item.name }</div>
    })
    return (
      <div>
        { weather }
        <Carousel effect="fade" className="carousel">
          { module }
        </Carousel>
        { personalInformation }
      </div>
    )
  }
}
