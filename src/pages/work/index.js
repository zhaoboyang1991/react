import React from 'react';
import { Link } from 'react-router-dom'

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to='/work/technology'>技术栈</Link>
          </li>
          <li>
            <Link to='/work/record'>履历</Link>
          </li>
          <li>
            <Link to='/work/blog'>博客</Link>
          </li>
        </ul>
      </div>
    )
  }
}
