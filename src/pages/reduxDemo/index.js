import React from 'react'
import { Input,Button,List } from 'antd'
import store from '../../redux/store/index'
import { getAddItemAction, deleteItemAction, getChangeItemAction } from '../../redux/action/index'

class TodoList extends React.Component {
	constructor(props) {
	  super(props);
		this.state = store.getState();  // 取store,赋值state
		store.subscribe(this.handleStoreChange) // 绑定监听回调
	}
	
	render(){
		const { inputValue, list } = this.state
		return (
      <div>
        <Input onChange={ this.handleInputChange } placeholder={ inputValue } style={{width:'300px',marginLeft:'10px',marginTop:'10px'}} />
        <Button onClick={ this.handleAddItem } type="primary" style={{marginLeft:'10px',marginTop:'10px'}} >提交</Button>
        <List style={{marginLeft:'10px',marginTop:'10px',width:'300px'}}
          bordered
          dataSource={ list }
          renderItem={(item, index) => (<List.Item onClick={ this.deleteItemClick.bind(this,index) }>{item}</List.Item> )}
      />
      </div>
    )
	}
	
	handleInputChange = (e) => {
		const action = getChangeItemAction(e.target.value); // 生成action
		store.dispatch(action); // dispatch(action)
	}
	
	handleAddItem = () => {
		const action = getAddItemAction();
		store.dispatch(action);
	}
	
	deleteItemClick(index) {
		const action = deleteItemAction(index);
		store.dispatch(action);
	} 
	
	handleStoreChange = () => {
		this.setState(store.getState())
	}
}

export default TodoList