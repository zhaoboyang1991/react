import React from 'react';
import { Link } from 'react-router-dom'

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to='/live/travel/time'>按时间</Link>
          </li>
          <li>
            <Link to='/live/travel/city'>按城市</Link>
          </li>
        </ul>
      </div>
    )
  }
}
