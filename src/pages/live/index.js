import React from 'react';
import { Link } from 'react-router-dom'

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to='/live/travel'>旅游</Link>
          </li>
          <li>
            <Link to='/live/loseWeight'>减肥</Link>
          </li>
          <li>
            <Link to='/live'>其他</Link>
          </li>
        </ul>
      </div>
    )
  }
}
