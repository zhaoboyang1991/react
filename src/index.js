import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import { createStore } from 'redux'

const reducer = (state = 0, action) => {
  switch (action.type) {
      case 'INCREMENT': return state + 1
      case 'DECREMENT': return state - 1
      default: return state
  }
}

const store = createStore(reducer)

const render = () => {
  ReactDOM.render(
    <App/>,
    document.getElementById('root')
  )
}

render()

store.subscribe(render)

export default store


